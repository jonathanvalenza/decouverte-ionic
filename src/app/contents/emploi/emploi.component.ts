import { Component, OnInit } from '@angular/core';
import { EmploiService } from 'src/app/services/emploi.service';


@Component({
  selector: 'app-emploi',
  templateUrl: './emploi.component.html',
  styleUrls: ['./emploi.component.scss'],
})
export class EmploiComponent implements OnInit {
  emplois;
  constructor(private emploiService: EmploiService) { }

  ngOnInit() {
    this.emploiService.getEmploi().subscribe((result) => {
      this.emplois = result;
    });
  }


}
