import { Component, OnInit } from '@angular/core';
import { ServiceTestService } from 'src/app/services/service-test.service';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss'],
})
export class TestComponent implements OnInit {
  listeTest;
  listeTestId;

  constructor(private serviceTestService: ServiceTestService) { }

  ngOnInit() {
    this.serviceTestService.getFormation().subscribe((result) => {
      this.listeTest = result;
    });
    this.serviceTestService.getFormationById().subscribe((result) => {
      this.listeTestId = result;
    });
  }

}
