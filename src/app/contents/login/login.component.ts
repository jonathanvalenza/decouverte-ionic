import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  constructor(
    private loginService: LoginService) { }

    login = {
      email: '',
      password: ''
    };

  ngOnInit() {}

  valid() {
    this.loginService.login(this.login).subscribe((result) => {
      localStorage.setItem('tkn', result['tkn']);
    });
  }
}