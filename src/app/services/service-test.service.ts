import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServiceTestService {

  constructor(private http: HttpClient) { }

  getFormation() {
    return this.http.get('http://localhost:3000/formationDetail');
  }

  getFormationById() {
    return this.http.get('http://localhost:3000//formationDetailById/:id');
  }
}
