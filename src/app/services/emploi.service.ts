import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
//import { map, delay, repeat } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EmploiService {
  constructor(private http: HttpClient) { }
  // quand tu sauras implémenter des observables tu pourras t'occuper de ce qu'il y a en commentaire.
  // offres: Observable<Array<offre>>;

  // constructor(private http: HttpClient) {
  //   // tu arrivera a le faire marcher un jour...
  //   this.offres = this.http.get('http://localhost:3000/emploi').pipe(
  //     map(result => {
  //       return result;
  //   }),
  //   delay(1000),
  //   repeat()
  //   );
  //  }

  getEmploi() {
    return this.http.get('http://localhost:3000/emploi');
  }
}

