import { Injectable } from '@angular/core';
import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpHeaders
} from '@angular/common/http';
import { map } from 'rxjs/operators';


import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Injectable()
export class Bearerinterceptor implements HttpInterceptor {

    constructor (
        private router: Router
        ) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // on teste si il existe un token dans le localStorage
    if (localStorage.getItem('tkn') !== undefined) {
        // on modifie le header au niveau du clonage
        const newRequest = req.clone({
            setHeaders : {
                'Authorization': 'Bearer ' + localStorage.getItem('tkn')
            }
        });
        console.log(newRequest);
        return next.handle(newRequest);
    }
    return next.handle(req).pipe(
        map((event: HttpEvent<any>) => {
            if (event instanceof HttpResponse) {
                if (event.status === 401) {
                    console.log('erreur 401');
                    this.router.navigate(['/login']);
                }
            }
            return event;
        })
    );
  }
}

