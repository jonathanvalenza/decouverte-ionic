import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ServiceTestService } from './services/service-test.service';
import { TestComponent } from './contents/test/test.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { EmploiComponent } from './contents/emploi/emploi.component';
import { EmploiService } from './services/emploi.service';
import { LoginComponent } from './contents/login/login.component';
import { LoginService } from './services/login.service';
import { Bearerinterceptor } from './interceptors/bearerinterceptor';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
    EmploiComponent,
    LoginComponent
  ],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule, HttpClientModule, FormsModule],
  providers: [
    StatusBar,
    SplashScreen,
    ServiceTestService,
    EmploiService,
    LoginService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: HTTP_INTERCEPTORS, useClass: Bearerinterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
